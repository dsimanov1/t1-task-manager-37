package ru.t1.simanov.tm.comparator;

import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.api.model.IHasName;

import java.util.Comparator;

public enum NameComparator implements Comparator<IHasName> {

    INSTANCE;

    public int compare(
            @Nullable final IHasName o1,
            @Nullable final IHasName o2
    ) {
        if (o1 == null || o2 == null) return 0;
        o1.getName();
        o2.getName();
        return o1.getName().compareTo(o2.getName());
    }

}
