package ru.t1.simanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.simanov.tm.api.repository.IProjectRepository;
import ru.t1.simanov.tm.api.repository.IUserRepository;
import ru.t1.simanov.tm.api.service.IConnectionService;
import ru.t1.simanov.tm.api.service.IPropertyService;
import ru.t1.simanov.tm.comparator.NameComparator;
import ru.t1.simanov.tm.marker.UnitCategory;
import ru.t1.simanov.tm.model.Project;
import ru.t1.simanov.tm.model.User;
import ru.t1.simanov.tm.service.ConnectionService;
import ru.t1.simanov.tm.service.PropertyService;

import java.sql.Connection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static ru.t1.simanov.tm.constant.ProjectTestData.*;
import static ru.t1.simanov.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.t1.simanov.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private static final IUserRepository userRepository = new UserRepository(connection);
    @NotNull
    private static String userId = "";
    @NotNull
    private final IProjectRepository repository = new ProjectRepository(connection);

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final User user = userRepository.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final User user = userRepository.findByLogin(USER_TEST_LOGIN);
        if (user != null) userRepository.remove(user);
    }

    @Before
    public void before() throws Exception {
        repository.add(userId, USER_PROJECT1);
        repository.add(userId, USER_PROJECT2);
    }

    @After
    public void after() throws Exception {
        repository.clear(userId);
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertNotNull(repository.add(userId, USER_PROJECT3));
        @Nullable final Project project = repository.findOneById(userId, USER_PROJECT3.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT3.getId(), project.getId());
        Assert.assertEquals(userId, project.getUserId());
    }

    @Test
    public void createByUserId() throws Exception {
        @NotNull final Project project = repository.create(userId, USER_PROJECT3.getName());
        Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
        Assert.assertEquals(userId, project.getUserId());
    }

    @Test
    public void createByUserIdWithDescription() throws Exception {
        @NotNull final Project project = repository.create(userId, USER_PROJECT3.getName(), USER_PROJECT3.getDescription());
        Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
        Assert.assertEquals(USER_PROJECT3.getDescription(), project.getDescription());
        Assert.assertEquals(userId, project.getUserId());
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<Project> projects = repository.findAll(userId);
        Assert.assertEquals(2, projects.size());
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void findAllComparatorByUserId() throws Exception {
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        final List<Project> projects = repository.findAll(userId, comparator);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(repository.existsById(NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(repository.existsById(USER_PROJECT1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertFalse(repository.existsById(userId, NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(repository.existsById(userId, USER_PROJECT1.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertNull(repository.findOneById(NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = repository.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertNull(repository.findOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = repository.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void clearByUserId() throws Exception {
        repository.clear(userId);
        Assert.assertEquals(0, repository.getCount(userId));
    }

    @Test
    public void remove() throws Exception {
        @Nullable final Project removedProject = repository.remove(USER_PROJECT2);
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(USER_PROJECT2.getId(), removedProject.getId());
        Assert.assertNull(repository.findOneById(removedProject.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {
        @Nullable final Project removedProject = repository.remove(userId, USER_PROJECT2);
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(USER_PROJECT2.getId(), removedProject.getId());
        Assert.assertNull(repository.findOneById(removedProject.getId()));
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertEquals(2, repository.getCount(userId));
    }

    @Test
    public void removeAll() throws Exception {
        repository.removeAll(PROJECT_LIST);
        Assert.assertEquals(0, repository.getCount(userId));
    }

    @Test
    public void update() throws Exception {
        USER_PROJECT1.setName(USER_PROJECT3.getName());
        repository.update(USER_PROJECT1);
        Assert.assertEquals(USER_PROJECT3.getName(), repository.findOneById(userId, USER_PROJECT1.getId()).getName());
    }

}
