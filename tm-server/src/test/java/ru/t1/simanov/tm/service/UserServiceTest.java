package ru.t1.simanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.simanov.tm.api.service.*;
import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.exception.entity.UserNotFoundException;
import ru.t1.simanov.tm.exception.field.IdEmptyException;
import ru.t1.simanov.tm.exception.field.LoginEmptyException;
import ru.t1.simanov.tm.exception.field.PasswordEmptyException;
import ru.t1.simanov.tm.exception.user.ExistsEmailException;
import ru.t1.simanov.tm.exception.user.ExistsLoginException;
import ru.t1.simanov.tm.exception.user.RoleEmptyException;
import ru.t1.simanov.tm.marker.UnitCategory;
import ru.t1.simanov.tm.model.User;

import static ru.t1.simanov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService service = new UserService(propertyService, connectionService, projectService, taskService);

    @Before
    public void before() throws Exception {
        service.add(USER_TEST);
    }

    @After
    public void after() throws Exception {
        @Nullable User user = service.findOneById(USER_TEST.getId());
        if (user != null) service.remove(user);
        user = service.findByLogin(USER_TEST_LOGIN);
        if (user != null) service.remove(user);
        user = service.findOneById(ADMIN_TEST.getId());
        if (user != null) service.remove(user);
        user = service.findByLogin(ADMIN_TEST_LOGIN);
        if (user != null) service.remove(user);
    }

    @Test
    public void add() throws Exception {
        Assert.assertNotNull(service.add(ADMIN_TEST));
        @Nullable final User user = service.findOneById(ADMIN_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST.getId(), user.getId());
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(service.existsById(""));
        Assert.assertFalse(service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(service.existsById(USER_TEST.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById("");
        });
        Assert.assertNull(service.findOneById(NON_EXISTING_USER_ID));
        @Nullable final User user = service.findOneById(USER_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
    }

    @Test
    public void removeById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById("");
        });
        Assert.assertNull(service.removeById(NON_EXISTING_USER_ID));
        @Nullable final User createdUser = service.add(ADMIN_TEST);
        @Nullable final User removedUser = service.removeById(ADMIN_TEST.getId());
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_TEST.getId(), removedUser.getId());
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId()));
    }

    @Test
    public void create() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create(null, ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create("", ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(ExistsLoginException.class, () -> {
            service.create(USER_TEST_LOGIN, ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_TEST_LOGIN, null);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_TEST_LOGIN, "");
        });
        @NotNull final User user = service.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD);
        Assert.assertEquals(user.getId(), service.findOneById(user.getId()).getId());
        Assert.assertEquals(ADMIN_TEST_LOGIN, user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    public void createWithEmail() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create(null, ADMIN_TEST_PASSWORD, ADMIN_TEST_EMAIL);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create("", ADMIN_TEST_PASSWORD, ADMIN_TEST_EMAIL);
        });
        Assert.assertThrows(ExistsLoginException.class, () -> {
            service.create(USER_TEST_LOGIN, ADMIN_TEST_PASSWORD, ADMIN_TEST_EMAIL);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_TEST_LOGIN, null, ADMIN_TEST_EMAIL);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_TEST_LOGIN, "", ADMIN_TEST_EMAIL);
        });
        Assert.assertThrows(ExistsEmailException.class, () -> {
            service.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, USER_TEST_EMAIL);
        });
        @NotNull final User user = service.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, ADMIN_TEST_EMAIL);
        Assert.assertEquals(user.getId(), service.findOneById(user.getId()).getId());
        Assert.assertEquals(ADMIN_TEST_LOGIN, user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(ADMIN_TEST_EMAIL, user.getEmail());
    }

    @Test
    public void createWithRole() throws Exception {
        @NotNull final Role role = Role.ADMIN;
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create(null, ADMIN_TEST_PASSWORD, role);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create("", ADMIN_TEST_PASSWORD, role);
        });
        Assert.assertThrows(ExistsLoginException.class, () -> {
            service.create(USER_TEST_LOGIN, ADMIN_TEST_PASSWORD, role);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_TEST_LOGIN, null, role);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_TEST_LOGIN, "", role);
        });
        Assert.assertThrows(RoleEmptyException.class, () -> {
            @NotNull final Role nullRole = null;
            service.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, nullRole);
        });
        @NotNull final User user = service.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, Role.ADMIN);
        Assert.assertEquals(user.getId(), service.findOneById(user.getId()).getId());
        Assert.assertEquals(ADMIN_TEST_LOGIN, user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void findByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.findByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.findByLogin("");
        });
        @Nullable final User user = service.findByLogin(USER_TEST_LOGIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
    }

    @Test
    public void remove() throws Exception {
        service.add(ADMIN_TEST);
        @Nullable final User removedUser = service.remove(ADMIN_TEST);
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_TEST.getId(), removedUser.getId());
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId()));
    }

    @Test
    public void removeByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.removeByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.removeByLogin("");
        });
        Assert.assertThrows(UserNotFoundException.class, () -> {
            service.removeByLogin(NON_EXISTING_USER_ID);
        });
        service.add(ADMIN_TEST);
        service.removeByLogin(ADMIN_TEST_LOGIN);
        Assert.assertNull(service.findOneById(ADMIN_TEST.getId()));
    }

    @Test
    public void setPassword() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.setPassword(null, ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.setPassword("", ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.setPassword(USER_TEST.getId(), null);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.setPassword(USER_TEST.getId(), "");
        });
        Assert.assertThrows(UserNotFoundException.class, () -> {
            service.setPassword(NON_EXISTING_USER_ID, ADMIN_TEST_PASSWORD);
        });
        service.setPassword(USER_TEST.getId(), ADMIN_TEST_PASSWORD);
        @NotNull final User user = service.findOneById(USER_TEST.getId());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        service.setPassword(USER_TEST.getId(), USER_TEST_PASSWORD);
    }

    @Test
    public void updateUser() throws Exception {
        @NotNull final String firstName = "User_first_name";
        @NotNull final String lastName = "User_last_name";
        @NotNull final String middleName = "User_middle_name";
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateUser(null, firstName, lastName, middleName);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateUser("", firstName, lastName, middleName);
        });
        service.updateUser(USER_TEST.getId(), firstName, lastName, middleName);
        @NotNull final User user = service.findOneById(USER_TEST.getId());
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());
    }

    @Test
    public void isLoginExists() throws Exception {
        Assert.assertFalse(service.isLoginExists(null));
        Assert.assertFalse(service.isLoginExists(""));
        Assert.assertTrue(service.isLoginExists(USER_TEST_LOGIN));
    }

    @Test
    public void isEmailExists() throws Exception {
        Assert.assertFalse(service.isEmailExists(null));
        Assert.assertFalse(service.isEmailExists(""));
        Assert.assertTrue(service.isEmailExists(USER_TEST_EMAIL));
    }

    @Test
    public void lockUserByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.lockUserByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.lockUserByLogin("");
        });
        Assert.assertThrows(UserNotFoundException.class, () -> {
            service.lockUserByLogin(NON_EXISTING_USER_ID);
        });
        service.lockUserByLogin(USER_TEST_LOGIN);
        @NotNull final User user = service.findOneById(USER_TEST.getId());
        Assert.assertTrue(user.getLocked());
    }

    @Test
    public void unlockUserByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.unlockUserByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.unlockUserByLogin("");
        });
        Assert.assertThrows(UserNotFoundException.class, () -> {
            service.unlockUserByLogin(NON_EXISTING_USER_ID);
        });
        service.lockUserByLogin(USER_TEST_LOGIN);
        service.unlockUserByLogin(USER_TEST_LOGIN);
        @NotNull final User user = service.findByLogin(USER_TEST_LOGIN);
        Assert.assertFalse(user.getLocked());
    }

}
