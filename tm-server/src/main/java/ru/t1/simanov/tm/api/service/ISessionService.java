package ru.t1.simanov.tm.api.service;

import ru.t1.simanov.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
