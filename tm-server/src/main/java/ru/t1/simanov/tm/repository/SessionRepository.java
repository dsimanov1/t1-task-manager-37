package ru.t1.simanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.api.DBConstants;
import ru.t1.simanov.tm.api.repository.ISessionRepository;
import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    protected String getTableName() {
        return DBConstants.TABLE_SESSION;
    }

    @NotNull
    @Override
    public Session fetch(@NotNull final ResultSet row) throws Exception {
        @NotNull final Session session = new Session();
        session.setId(row.getString(DBConstants.COLUMN_ID));
        session.setDate(row.getTimestamp(DBConstants.COLUMN_DATE));
        session.setUserId(row.getString(DBConstants.COLUMN_USER_ID));
        session.setRole(Role.valueOf(row.getString(DBConstants.COLUMN_ROLE)));
        return session;
    }

    @NotNull
    @Override
    public Session add(@NotNull final Session session) throws Exception {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s) VALUES (?, ?, ?, ?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_DATE,
                DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_ROLE
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getId());
            statement.setTimestamp(2, new Timestamp(session.getDate().getTime()));
            statement.setString(3, session.getUserId());
            statement.setString(4, session.getRole().toString());
            statement.executeUpdate();
        }
        return session;
    }

    @NotNull
    @Override
    public Session add(@NotNull final String userId, @NotNull final Session session) throws Exception {
        session.setUserId(userId);
        return add(session);
    }

    @Override
    public void update(@NotNull final Session session) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstants.COLUMN_DATE, DBConstants.COLUMN_USER_ID,
                DBConstants.COLUMN_ROLE, DBConstants.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setTimestamp(1, new Timestamp(session.getDate().getTime()));
            statement.setString(2, session.getUserId());
            statement.setString(3, session.getRole().toString());
            statement.setString(4, session.getId());
            statement.executeUpdate();
        }
    }

}
