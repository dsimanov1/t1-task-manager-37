package ru.t1.simanov.tm.constant;

import org.jetbrains.annotations.NotNull;

public class ProjectTestData {

    @NotNull
    public final static String USER_PROJECT1_NAME = "User Test Project 1";

    @NotNull
    public final static String USER_PROJECT1_DESCRIPTION = "User Test Project Description 1";

    @NotNull
    public final static String USER_PROJECT2_NAME = "User Test Project 2";

    @NotNull
    public final static String USER_PROJECT2_DESCRIPTION = "User Test Project Description 2";

    @NotNull
    public final static String USER_PROJECT3_NAME = "User Test Project 3";

    @NotNull
    public final static String USER_PROJECT3_DESCRIPTION = "User Test Project Description 3";

}
