package ru.t1.simanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.dto.request.TaskListRequest;
import ru.t1.simanov.tm.enumerated.Sort;
import ru.t1.simanov.tm.model.Task;
import ru.t1.simanov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Display task list.";

    @NotNull
    public static final String NAME = "task-list";

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest(getToken());
        request.setSort(sort);
        @Nullable final List<Task> tasks = getTaskEndpoint().listTask(request).getTasks();
        if (tasks == null) return;
        @NotNull final int[] index = {1};
        tasks.forEach(m -> {
            System.out.println(index[0] + ". " + m);
            index[0]++;
        });
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
